var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
var ddb = new AWS.DynamoDB.DocumentClient();
var ses = new AWS.SES({ region: "ap-south-1" });
var sesv2 = new AWS.SESV2({ region: "ap-south-1" });
var uuid = require("uuid");

module.exports.handler = async (event) => {
  if (event.input.action == "addEmail") {
    var params = {
      EmailAddress: event.input.email,
    };
    await ses.verifyEmailIdentity(params).promise();

    let addEmail = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Item: {
        id: uuid.v4(),
        status: "live",
        type: event.input.type,
        date: formatDate(),
        addedBy: event.input.addedBy,
        email: event.input.email,
        name: event.input.name,
        phoneNumber: event.input.phoneNumber,
        group: event.input.group,
        role: event.input.role,
        notes: event.input.notes,
      },
    };
    await ddb.put(addEmail).promise();
  } else if (event.input.action == "listEmail") {
    var paramsList = {
      IdentityType: "EmailAddress",
      MaxItems: 10,
    };
    let listIDsPromise = await ses.listIdentities(paramsList).promise();

    var paramsStatus = {
      Identities: listIDsPromise.Identities,
    };
    let resStatus = await ses
      .getIdentityVerificationAttributes(paramsStatus)
      .promise();


    let params = {
      TableName: process.env.ALLMESHDETAILTABLE,
      IndexName: "type-status-index",
      KeyConditionExpression: "#type1 = :type and #status1 = :status",
      ExpressionAttributeNames: {
        "#type1": "type",
        "#status1": "status",
      },
      ExpressionAttributeValues: {
        ":type": "Email",
        ":status": "live",
      },
    };

    let res = await ddb.query(params).promise();

    res.Items.map((item) => {
      item.verificationStatus =
        resStatus.VerificationAttributes[item.email].VerificationStatus;
    });
    return { output: res.Items };
  } else if (event.input.action == "deleteEmail") {
    var params = {
      EmailIdentity: event.input.email,
    };
    await sesv2.deleteEmailIdentity(params).promise();
    let deleteMail = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression: "set #status1 = :status",
      ExpressionAttributeNames: {
        "#status1": "status",
      },
      ExpressionAttributeValues: {
        ":status": "deleted",
      },
    };

    let res = await ddb.update(deleteMail).promise();
    return { output: res.Items };
  }
};

function formatDate() {
  var d = new Date(),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}
