var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
var ddb = new AWS.DynamoDB.DocumentClient();
var ses = new AWS.SES({ region: "ap-south-1" });
var sns = new AWS.SNS({ region: "ap-south-1" });
var moment = require("moment");

module.exports.handler = async (event) => {
  //updating order status

  if (event.input.status === "rejected") {
    var updateStatus = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression:
        "SET #status = :status, #rejectReason = :rejectReason, #statusUpdatedDate = :statusUpdatedDate",
      ExpressionAttributeNames: {
        "#status": "status",
        "#rejectReason": "rejectReason",
        "#statusUpdatedDate": "statusUpdatedDate",
      },
      ExpressionAttributeValues: {
        ":status": event.input.status,
        ":rejectReason": event.input.rejectReason,
        ":statusUpdatedDate": formatDate(),
      },
    };
    await ddb.update(updateStatus).promise();
  } else {
    var updateStatus = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression:
        "SET #status = :status, #updatedBy = :updatedBy, #statusUpdatedDate = :statusUpdatedDate, #estimatedDelivery = :estimatedDelivery",
      ExpressionAttributeNames: {
        "#status": "status",
        "#updatedBy": "updatedBy",
        "#statusUpdatedDate": "statusUpdatedDate",
        "#estimatedDelivery": "estimatedDelivery",
      },
      ExpressionAttributeValues: {
        ":status": event.input.status,
        ":updatedBy": event.input.updatedBy,
        ":statusUpdatedDate": formatDate(),
        ":estimatedDelivery": event.input.estimatedDelivery,
      },
    };
    await ddb.update(updateStatus).promise();
  }

  let orderParams = {
    TableName: process.env.ALLMESHDETAILTABLE,
    Key: { id: event.input.id },
  };

  let order = await ddb.get(orderParams).promise();

  if (
    event.input.status === "inProduction" ||
    event.input.status === "outForDelivery"
  ) {
    var params = {
      Destination: {
        ToAddresses: event.input.vendorList,
      },
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `<html>
                  <head>
                  <style>
                  table, th, td {
                    border: 1px solid black;
                    border-collapse: collapse;
                  }
                  </style>
                  </head>
                  <body>
                  <h4>Hello!  ${order.Item.customerName}</h4>
                  <p>Greetings from AllMesh.</p>
                  <p>Order Id : ${order.Item.customerReferenceNumber}</p>
                  <p>Customer Name : ${order.Item.customerName}</p>
                  <p>Shipping Address : ${order.Item.shippingAddress}</p>
                  <p/>
                  <p>Below are the order Details</p>
                  <table style="width:80%">
                  <tr>
                    <th style="width:30%">Product Name</th>
                    <th style="width:10%">Width (mm)</th>
                    <th style="width:10%">Height (mm)</th>
                    <th style="width:10%">Qty</th>
                    <th style="width:10%">Location</th>
                    <th style="width:10%">Type</th>
                    <th style="width:10%">Unit Price</th>
                    <th style="width:10%">Total</th>
                  </tr>
                  ${Object.entries(order.Item.orderDetails).map(
                    ([key, item]) => `
                  <tr>
                    <td style="width:30%">${item.productName}(${
                      item.productDescription
                    })</td>
                    <td style="width:10%">${item.width}</td>
                    <td style="width:10%">${item.height}</td>
                    <td style="width:10%">${item.quantity}</td>
                    <td style="width:10%">${item.productLocation}</td>
                    <td style="width:10%">${item.productType}</td>
                    <td style="width:10%">${item.itemPrice.toFixed(2)}</td>
                    <td style="width:10%">${item.itemTotal.toFixed(2)}</td>
                  </tr>`
                  )}
                </table> 
                  </body>
                  </html>`,
          },
          Text: {
            Data: `Greetings from AllMesh,\n\nOrder Id : ${
              order.Item.customerReferenceNumber
            }\nCustomer Name : ${order.Item.customerName}
             \nShipping Address : ${
               order.Item.shippingAddress
             }\nOrder Details :\n${Object.entries(order.Item.orderDetails).map(
              ([key, item], index) =>
                index +
                1 +
                ") " +
                item.productName +
                "(" +
                item.productDescription +
                ")" +
                ", " +
                item.width +
                "mm width * " +
                item.height +
                "mm height * " +
                item.panelCount +
                " panels/doors " +
                item.itemPrice.toFixed(2) +
                " * " +
                item.quantity +
                " Total " +
                item.itemTotal.toFixed(2) +
                "\n"
            )}\n`,
          },
        },

        Subject: {
          Data: "Order Details: " + order.Item.customerReferenceNumber,
        },
      },
      Source: "smita@ghoshak.com",
    };

    await ses.sendEmail(params).promise();
  }

  //sending text message to customer
  if (
    event.input.status === "inProduction" ||
    event.input.status === "outForDelivery" ||
    event.input.status === "delivered" ||
    event.input.status === "installed"
  ) {
    let msg =
      "Dear " +
      order.Item.customerName +
      ", your order " +
      order.Item.customerReferenceNumber;
    let statusMsg = "";
    if (event.input.status === "inProduction")
      statusMsg =
        " is in production, estimated delivery date is " +
        moment(event.input.estimatedDelivery, "YYYY-MM-DD").format("D MMM") +
        ".";
    else if (event.input.status === "outForDelivery")
      statusMsg = " is out for delivery.";
    else if (event.input.status === "delivered") statusMsg = " is delivered.";
    else if (event.input.status === "installed") statusMsg = " is installed.";
    const params = {
      Message: msg + statusMsg,
      PhoneNumber: order.Item.customerPhone,
    };

    console.log(params);

    await sns.publish(params).promise();
  }
};

function formatDate() {
  var d = new Date(),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}
