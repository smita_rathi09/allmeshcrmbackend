var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
var ddb = new AWS.DynamoDB.DocumentClient();
var sns = new AWS.SNS({ region: "ap-south-1" });
var uuid = require("uuid");
var moment = require("moment");

module.exports.handler = async (event) => {
  //updating order status

  if (event.input.status === "rejected") {
    var updateStatus = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression:
        "SET #status = :status, #rejectReason = :rejectReason, #statusUpdatedDate = :statusUpdatedDate",
      ExpressionAttributeNames: {
        "#status": "status",
        "#rejectReason": "rejectReason",
        "#statusUpdatedDate": "statusUpdatedDate",
      },
      ExpressionAttributeValues: {
        ":status": event.input.status,
        ":rejectReason": event.input.rejectReason,
        ":statusUpdatedDate": formatDate(),
      },
    };
    await ddb.update(updateStatus).promise();
  } else if (event.input.status === "visitScheduled") {
    var updateStatus = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression:
        "SET #status = :status, #visitDate = :visitDate, #visitTime = :visitTime, #updatedBy = :updatedBy, #statusUpdatedDate = :statusUpdatedDate",
      ExpressionAttributeNames: {
        "#status": "status",
        "#visitDate": "visitDate",
        "#visitTime": "visitTime",
        "#updatedBy": "updatedBy",
        "#statusUpdatedDate": "statusUpdatedDate",
      },
      ExpressionAttributeValues: {
        ":status": event.input.status,
        ":visitDate": event.input.visitDate,
        ":visitTime": event.input.visitTime,
        ":updatedBy": event.input.updatedBy,
        ":statusUpdatedDate": formatDate(),
      },
    };
    await ddb.update(updateStatus).promise();
  } else {
    var updateStatus = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Key: { id: event.input.id },
      UpdateExpression:
        "SET #status = :status, #updatedBy = :updatedBy, #statusUpdatedDate = :statusUpdatedDate",
      ExpressionAttributeNames: {
        "#status": "status",
        "#updatedBy": "updatedBy",
        "#statusUpdatedDate": "statusUpdatedDate",
      },
      ExpressionAttributeValues: {
        ":status": event.input.status,
        ":updatedBy": event.input.updatedBy,
        ":statusUpdatedDate": formatDate(),
      },
    };
    await ddb.update(updateStatus).promise();

    let createOrder = {
      TableName: process.env.ALLMESHDETAILTABLE,
      Item: {
        id: uuid.v4(),
        status: "live",
        type: "Order",
        addedDate: formatDate(),
        customerName: event.input.customerName,
        customerPhone: event.input.customerPhone,
        customerEmail: event.input.customerEmail,
        billingAddress: event.input.billingAddress,
        shippingAddress: event.input.shippingAddress,
        orderDetails: event.input.orderDetails,
        notes: event.input.notes,
        visitDate: event.input.visitDate,
        visitTime: event.input.visitTime,
        updatedBy: event.input.updatedBy,
        customerReferenceNumber: event.input.customerReferenceNumber,
        estimatedAmount: event.input.estimatedAmount,
        quotationId: event.input.id,
        subTotal: event.input.subTotal,
        GST: event.input.GST,
        GSTAmount: event.input.GSTAmount,
        discountValue: event.input.discountValue,
        discountType: event.input.discountType,
        discountAmount: event.input.discountAmount,
        time: event.input.time,
      },
    };
    await ddb.put(createOrder).promise();
  }

  //sending text message to customer
  if (
    event.input.status === "visitScheduled" ||
    event.input.status === "convertedToOrder"
  ) {
    let msg =
      "Dear " +
      event.input.customerName +
      ", your quotation " +
      event.input.customerReferenceNumber;
    let statusMsg = "";
    if (event.input.status === "visitScheduled")
      statusMsg =
        " visit is scheduled on " +
        moment(event.input.visitDate, "YYYY-MM-DD").format("D MMM") +
        ".";
    else if (event.input.status === "convertedToOrder")
      statusMsg = " is converted to order.";
    const params = {
      Message: msg + statusMsg,
      PhoneNumber: event.input.customerPhone,
    };

    await sns.publish(params).promise();
  }
};

function formatDate() {
  var d = new Date(),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}
