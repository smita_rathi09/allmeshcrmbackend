var AWS = require("aws-sdk");
var sns = new AWS.SNS();
AWS.config.update({ region: "ap-south-1" });
var db = new AWS.DynamoDB.DocumentClient();

exports.handler = async event => {
  let token = {};

  if (event.input == null || event.input == "") {
    token = event.token.token;
  } else {
    token = event.input.token.token;
  }
  var params = {
    PlatformApplicationArn:
      "arn:aws:sns:ap-south-1:802791315112:app/GCM/ghoshak",
    Token: token
  };

  return sns.createPlatformEndpoint(params).promise();

  
};
