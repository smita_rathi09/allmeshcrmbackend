var AWS = require("aws-sdk");
AWS.config.update({ region: "ap-south-1" });
var ddb = new AWS.DynamoDB.DocumentClient();
var moment = require("moment");

module.exports.handler = async () => {
  //updating order status

  var expiredQuotaionParams = {
    TableName: process.env.ALLMESHDETAILTABLE,
    IndexName: "type-status-index",
    KeyConditionExpression: "#status = :live and #type = :type",
    FilterExpression: "#addedDate < :today",
    ExpressionAttributeNames: {
      "#status": "status",
      "#addedDate": "addedDate",
      "#type": "type",
    },
    ExpressionAttributeValues: {
      ":today": moment().subtract(15, "days").format("YYYY-MM-DD"),
      ":live": "live",
      ":type": "Quotation",
    },
  };

  var expiredQuotaionList = await ddb.query(expiredQuotaionParams).promise();
  console.log("expiredQuotaionList " + expiredQuotaionList.Items.length);
  await BatchUpdateItems(expiredQuotaionList.Items, "status", "expired");
};

const BatchUpdateItems = async (
  arrayItems,
  updateAttribute,
  updateAttributeValue
) => {
  return await new Promise(async (resolve) => {
    let count = 0;
    do {
      await Promise.all(
        arrayItems.slice(count * 25, count * 25 + 25).map(async (item) => {
          var paramsUpdate = {
            TableName: process.env.ALLMESHDETAILTABLE,
            Key: {
              id: item.id,
            },
            UpdateExpression: `set #${updateAttribute} = :${updateAttribute}`,

            ExpressionAttributeNames: {
              [`#${updateAttribute}`]: updateAttribute,
            },

            ExpressionAttributeValues: {
              [`:${updateAttribute}`]: updateAttributeValue,
            },
          };
          // console.log(paramsUpdate)
          return await ddb.update(paramsUpdate).promise();
        })
      );

      count++;
    } while (arrayItems.length - count * 25 > 0);

    resolve();
  });
};
