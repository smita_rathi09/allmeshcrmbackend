const AWS = require("aws-sdk");
// const sns = new SNS();
var sns = new AWS.SNS({ region: "ap-south-1" });
var crypto = require("crypto");
var pinpoint = new AWS.Pinpoint({ apiVersion: "2016-12-01" });

exports.handler = async (event) => {
  let secretLoginCode;
  let code;
  if (!event.request.session || !event.request.session.length) {
    var phoneNumber = event.request.userAttributes.phone_number;

    code = crypto.randomBytes(5).join("");
    secretLoginCode = parseInt(code.toString("hex"), 16)
      .toString()
      .substr(0, 6);

    await sendSMSViaSNS(phoneNumber, secretLoginCode);
  } else {
    // There's an existing session. Don't generate new digits but
    // re-use the code from the current session. This allows the user to
    // make a mistake when keying in the code and to then retry, rather
    // the needing to e-mail the user an all new code again.
    const previousChallenge = event.request.session.slice(-1)[0];
    secretLoginCode = previousChallenge.challengeMetadata.match(
      /CODE-(\d*)/
    )[1];
  }

  // This is sent back to the client app
  event.response.publicChallengeParameters = {
    phone: event.request.userAttributes.phone_number,
  };

  // Add the secret login code to the private challenge parameters
  // so it can be verified by the "Verify Auth Challenge Response" trigger
  event.response.privateChallengeParameters = { secretLoginCode };

  // Add the secret login code to the session so it is available
  // in a next invocation of the "Create Auth Challenge" trigger
  event.response.challengeMetadata = `CODE-${secretLoginCode}`;

  return event;
};

async function sendSMSViaSNS(phoneNumber, secretLoginCode) {
  const params = {
    Message: `${secretLoginCode} is your OTP (One Time Password) to authenticate your login to GHOSHAK app. Do not share it with anyone.`,
    PhoneNumber: phoneNumber,
  };
  await sns.setSMSAttributes({
    attributes: {
      EntityId: "1001891018903590424",
      TemplateId: "1007408193576698829",
    },
  });
  await sns.publish(params).promise();

  // var destinationNumber = phoneNumber;
  // var message = `${secretLoginCode} is your OTP (One Time Password) to authenticate your login to GHOSHAK app. Do not share it with anyone.`;
  // var applicationId = "77ae7be7e3904e83a0be80123fa761e9";
  // var messageType = "TRANSACTIONAL";
  // var senderId = "GHOSHK";

  // var params = {
  //   ApplicationId: applicationId,
  //   MessageRequest: {
  //     Addresses: {
  //       [destinationNumber]: {
  //         ChannelType: "SMS",
  //       },
  //     },
  //     MessageConfiguration: {
  //       SMSMessage: {
  //         Body: message,
  //         MessageType: messageType,
  //         SenderId: senderId,
  //         EntityId: "1001891018903590424",
  //         TemplateId: "1007408193576698829",
  //       },
  //     },
  //   },
  // };
  // let result = await pinpoint.sendMessages(params).promise();

  // return result;
}
